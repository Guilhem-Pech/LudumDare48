using GameEvent;
using GameStates;
using Manager;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    
    public void PlayGame ()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        GameManager.Instance.gameObject.SendEvent(new OnStartGame());
    }

    public void QuitGame ()
    {
        Application.Quit();
    }

    
}
