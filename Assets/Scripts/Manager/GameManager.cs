using System;
using System.Collections.Generic;
using System.Linq;
using Entities.Insects;
using Entities.Spawnpoints;
using GameEvent;
using Player;
using RGSMS.Scene;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
#endif
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Utils;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }
        public List<InsectSetting> poolOfInsects = new List<InsectSetting>();
        public List<InsectSetting> insectToCollect= new List<InsectSetting>();
        [ShowInInspector]
        public HashSet<InsectSetting> insectCollected= new HashSet<InsectSetting>();
        public List<Spawnpoint> spawnpointList= new List<Spawnpoint>();
        public PlayerController localPlayer;
        public SceneInspector backgroundScene = null;
        [InfoBox("you can add your scene via Temp list")] [InlineButton(nameof(InvertList))]
        public List<string> poolScene = new List<string>();
        public int nbSceneInOrderBeforeRandom = 3;
        public Image screenOver;
        public int nextLevelIndex = 0;
        
        #region POOLSCENEEDITOR

        
        void InvertList()
        {
            poolScene.Reverse();
        }
        
#if UNITY_EDITOR
        
        [OnCollectionChanged ("OnChangeBefore","OnchangeAfter")]
        public List<SceneAsset> poolSceneTEMP = new List<SceneAsset>();

        public void OnchangeAfter(CollectionChangeInfo info, object value)
        {
            poolSceneTEMP = poolSceneTEMP.Distinct().ToList();
            poolScene = poolScene.Distinct().ToList();
        }

        public void OnChangeBefore(CollectionChangeInfo info, object value)
        {
            SceneAsset a = (SceneAsset) info.Value;
            switch (info.ChangeType)
            {
                case CollectionChangeType.Unspecified:
                    Debug.LogError(info);
                    break;
                case CollectionChangeType.Add:
                    poolScene.Add(a.name);
                    break;
                case CollectionChangeType.Insert:
                    poolScene.Add(a.name);
                    break;
                case CollectionChangeType.RemoveValue:
                    poolScene.Remove(a.name);
                    break;
                case CollectionChangeType.RemoveIndex:
                    poolScene.Remove(poolSceneTEMP[info.Index].name);
                    break;
                case CollectionChangeType.Clear:
                    poolScene.Clear();
                    break;
                case CollectionChangeType.RemoveKey:
                    Debug.Log("UNDIFINED");
                    break;
                case CollectionChangeType.SetKey:
                    Debug.Log("UNDIFINED");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
#endif
        #endregion
        
        void Start()
        {
            Assert.IsNull(Instance, "Attempt to re-create a Gamemanager while one is already instantiated");
            Instance = this;
            DontDestroyOnLoad(gameObject);
            gameObject.ListenToEvent<OnPlayerPickupInsect>(OnPickupInsect);
            gameObject.ListenToEvent<OnPlayerDie>(OnPlayerDie);
        }

        public void InitGame()
        {
            insectCollected.Clear();
            insectToCollect.Clear();
            Assert.IsTrue(poolOfInsects.Count >= 4);
            List<InsectSetting> copyPool = new List<InsectSetting>(poolOfInsects);
            while (insectToCollect.Count < 4) //This loop is the shittiest one I've ever made, I'm tired, please don't judge
            {
                InsectSetting insect = copyPool.GetRandom();
                insectToCollect.Add(insect);
                copyPool.Remove(insect);
            }
        }
        
        
        private void OnPlayerDie(IEvent obj)
        {
            OnPlayerDie myEvent = obj as OnPlayerDie;
            Assert.IsNotNull(myEvent, "myEvent != null"); 
            
            //TODO Respawn flow

        }

        private void OnPickupInsect(IEvent obj)
        {
            Sound.Instance.PlayGetInsect();
            OnPlayerPickupInsect myEvent = obj as OnPlayerPickupInsect;
            Assert.IsNotNull(myEvent, "myEvent != null"); 
            InsectSetting c = insectToCollect.FirstOrDefault(b => b.insectName == myEvent.insectName) ;
            if (c != null)
            {
                Sound.Instance.PlayUnlockInsect();
            }

            myEvent.player.AddLife();
            Destroy(myEvent.insect.gameObject);
           
        }
    }
}
