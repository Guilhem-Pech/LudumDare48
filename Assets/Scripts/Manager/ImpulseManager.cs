﻿using System;
using Cinemachine;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Assertions;

namespace Manager
{
    public class ImpulseManager : MonoBehaviour
    {
        [Required] public CinemachineImpulseSource source;

        public static ImpulseManager instance;
        
        private void Start()
        {
            Assert.IsTrue(instance == null, "You cannot have multiple ImpulseManager");
            instance = this;
        }
    }
}