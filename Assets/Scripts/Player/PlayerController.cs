using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cinemachine;
using Entities.Cracks;
using GameEvent;
using GameStates;
using Manager;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;
using Utils;

namespace Player
{
    public class OnPlayerDie : GameEvent.Event
    {
        public PlayerController player;
    }
    
    
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour
    {
        //SETTINGS
        public float accelerationMultiplier = 1f;
        public float autoStopAtSpeed = 0.5f;
        [SerializeField] private GameInputReader gameInputReader = default;
        public int maxLife = 3;
        public float invincibilityTime = 1f;

        // Public value for debug
        public int curLife = 0;
        
        //PRIVATE VALUES
        private Rigidbody2D _rigidbody;
        private Vector2 _axisInputs;
        private Collider2D _collider2D;
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private Vector2 _lastVelocity = Vector2.zero;
        private Vector2 _lastPosition = Vector2.zero;
        private Vector2 _acceleration = Vector2.zero;
        private PlayerInput _playerInput;
        public string CurrentControlScheme => _playerInput.currentControlScheme;
        
        private static readonly int XAxis = Animator.StringToHash("XAxis");
        private static readonly int YAxis = Animator.StringToHash("YAxis");
        private static readonly int AbsXAxis = Animator.StringToHash("AbsXAxis");
        private static readonly int AbsYAxis = Animator.StringToHash("AbsYAxis");
        private static readonly int HorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
        private static readonly int VerticalSpeed = Animator.StringToHash("VerticalSpeed");
        private bool _invincible;
        private static readonly int Stomp = Animator.StringToHash("Stomp");
        public Rigidbody2D Rigidbody { get ; private set; }

        //-- Functions
        private void OnMoveInput(Vector2 axis)
        {
            _axisInputs = axis;
        }

        public void OnMovePImput(InputAction.CallbackContext ctx)
        {
            if(ctx.performed)
                OnMoveInput(ctx.ReadValue<Vector2>());
        }
        
        public void OnStomp(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                OnStompEvent();
            }
        } 
        
        private void OnStompEvent()
        {
            //TODO Trigger Animation
            DisableMovement();
            _rigidbody.velocity = Vector2.zero;
            _animator.SetTrigger(Stomp);
            //OnStompHit(); //TODO Do a coroutine that call this after the stomp anim or add a trigger in the animation

            Sound.Instance.PlayStomp();
        }
        private void OnStompHit()
        {
            ContactFilter2D filter2D = new ContactFilter2D();
            var result = new List<Collider2D>();
            _collider2D.OverlapCollider(filter2D.NoFilter(), result);
            if (result.Any(collider => collider.TryGetComponent(out Cracks _)))
            {
                ImpulseManager.instance.source.GenerateImpulseAt(transform.position, Vector3.down);
                GameManager.Instance.gameObject.SendEvent(new OnPlayerStompOnCrack(){playerController = this});
            }
            EnableMovement();
        }
        
        private void OnPauseEvent()
        {
            //throw new NotImplementedException();
            Sound.Instance.PlayPause();
        }

        private void DisableMovement()
        {
            gameInputReader.moveEvent -= OnMoveInput;
            _axisInputs = Vector2.zero;
        }
        
        private void EnableMovement()
        {
            gameInputReader.moveEvent += OnMoveInput;
        }

        private void Start()
        {
            _playerInput = GetComponent<PlayerInput>();
            Assert.IsNull(GameManager.Instance.localPlayer);
            GameManager.Instance.localPlayer = this;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            curLife = maxLife;
            _rigidbody = GetComponent<Rigidbody2D>();
            Rigidbody = _rigidbody;
            _collider2D = GetComponent<Collider2D>();
            _animator = GetComponent<Animator>();
            // If this assert is throw, pass the collider that calculate the stomp in public
            Assert.IsTrue(_rigidbody.attachedColliderCount <= 1, "We need to change some logic here cause we use the Collider component");
            _lastPosition = _rigidbody.position;

            Sound.Instance.PlayPlay();

            
        }

        void OnEnable()
        {
            gameInputReader.moveEvent += OnMoveInput;
            gameInputReader.stompEvent += OnStompEvent;
            gameInputReader.pauseEvent += OnPauseEvent;

        }

        private void OnDisable()
        {
            gameInputReader.moveEvent -= OnMoveInput;
            gameInputReader.stompEvent -= OnStompEvent;
            gameInputReader.pauseEvent -= OnPauseEvent;


        }

        private void Update()
        {
            Vector2 curVelocity = _rigidbody.velocity;
            _animator.SetFloat(HorizontalSpeed, math.abs(curVelocity.x));
            _animator.SetFloat(VerticalSpeed, math.abs(curVelocity.y));

            if (!(_rigidbody.position - _lastPosition).IsZero())
            {
                Vector2 curDirection = (_rigidbody.position - _lastPosition).normalized;
                _animator.SetFloat(XAxis, curDirection.x);
                _animator.SetFloat(YAxis, curDirection.y);
                _animator.SetFloat(AbsXAxis, math.abs(curDirection.x));
                _animator.SetFloat(AbsYAxis, math.abs(curDirection.y)); 
            }
            _lastPosition = _rigidbody.position;
        }

        private void FixedUpdate()
        {
            _rigidbody.AddForce(_axisInputs * accelerationMultiplier, ForceMode2D.Impulse);
            //Snap velocity to 0 in order to stop the player when velocity is low
            if (_axisInputs.IsZero() && _rigidbody.velocity.MagnitudeLessThan(autoStopAtSpeed) )
            {
                _rigidbody.velocity = Vector2.zero;   
            }

            Vector2 velocity = _rigidbody.velocity;
            _acceleration = (_lastVelocity - velocity) / Time.fixedDeltaTime;
            _lastVelocity = velocity;
        }

        public void AddLife()
        {
            curLife = math.clamp(curLife + 1, 0, maxLife);
            GameManager.Instance.gameObject.SendEvent(new RefreshLifeHUD());
        }

        public void RemoveLife()
        {
            if (_invincible) return;
            curLife = math.clamp(curLife - 1, 0, maxLife);
            GameManager.Instance.gameObject.SendEvent(new RefreshLifeHUD());
            Sound.Instance.PlayLoseLife();
            if (curLife <= 0)
            {
                Die();
                return;
            }
            StartCoroutine(SetInvincibleFor(invincibilityTime));
        }

        private IEnumerator SetInvincibleFor(float time)
        {
            Assert.IsFalse(_invincible, "Already invincible !");
            _spriteRenderer.FlickerSprite(Color.red, 0.1f,time);
            _invincible = true;
            yield return new WaitForSeconds(time);
            _invincible = false;
        }
        
        private void Die()
        {
            Debug.Log("I died");
            OnPlayerDie dieEvent = new OnPlayerDie {player = this};
            GameManager.Instance.gameObject.SendEvent(dieEvent);

            Sound.Instance.PlayGameOver();
        }

        void OnCollisionEnter2D(Collision2D C)
        {
            Debug.Log ("Boum");

            if (!C.gameObject.CompareTag("NoImpulse"))
            {
                Sound.Instance.PlayWallHit();
            }

        }
    }
}
