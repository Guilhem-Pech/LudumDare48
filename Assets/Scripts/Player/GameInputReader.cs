﻿using System;
using Inputs;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Users;

namespace Player
{
    [CreateAssetMenu(fileName = "GameInputReader", menuName = "ScriptableObject/GameInputReader", order = 0)]
    public class GameInputReader : ScriptableObject, InputActions.IPlayerActions
    {
        public event UnityAction<Vector2> moveEvent = delegate {  }; 
        public event UnityAction stompEvent = delegate {  };
        public event UnityAction pauseEvent = delegate {  };
        private InputActions _inputActions;

        private void OnEnable()
        {
            
            if (_inputActions == null)
            {
                _inputActions = new InputActions();
            }
            _inputActions.Player.SetCallbacks(this);
            _inputActions.Enable();
            _inputActions.Player.Enable();
        }
        
        

        private void OnDisable()
        {
            _inputActions.Player.Disable();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                moveEvent.Invoke(context.ReadValue<Vector2>());
            }
        }

        public void OnPMovement(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                moveEvent.Invoke(context.ReadValue<Vector2>());
            }
        }

        public void OnStomp(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                stompEvent.Invoke();
            }
        }

        public void OnPause(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                pauseEvent.Invoke();
            }
        }
    }
}