﻿using System;
using System.Collections.Generic;
using Entities.Insects;
using GameEvent;
using Manager;
using UnityEngine;
using UnityEngine.UI;
using Event = GameEvent.Event;

namespace Player
{
    
    public class RefreshInsectHUD : Event{}

    public class RefreshLifeHUD : Event{}

    public class HUD : MonoBehaviour
    {
        public List<Image> lifeSlots;
        public List<Image> insectsSlots;
        private GameManager _manager;
        public void OnEnable()
        {
            _manager = GameManager.Instance;
            _manager.gameObject.ListenToEvent<RefreshInsectHUD>(RefreshInsectHUD);
            _manager.gameObject.ListenToEvent<RefreshLifeHUD>(RefreshLifeHUD);
        }

        private void OnDisable()
        {
            _manager.gameObject.UnregisterToEvent<RefreshInsectHUD>(RefreshInsectHUD);
            _manager.gameObject.UnregisterToEvent<RefreshLifeHUD>(RefreshLifeHUD);
        }

        private void RefreshLifeHUD(IEvent obj)
        {
            for (var i = 0; i < lifeSlots.Count; i++)
            {
                lifeSlots[i].color = (i+1) <= _manager.localPlayer.curLife ? Color.white : Color.clear;
            }
        }

        private void RefreshInsectHUD(IEvent obj)
        {
            for (var i = 0; i < _manager.insectToCollect.Count; ++i)
            {
                insectsSlots[i].sprite = _manager.insectToCollect[i].initialSprite;
                insectsSlots[i].color = _manager.insectCollected.Contains(_manager.insectToCollect[i]) ? Color.white : Color.black;
            }
        }
    }
}