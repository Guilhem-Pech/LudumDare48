﻿using GameEvent;
using Manager;
using RGSMS.Scene;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Event = GameEvent.Event;

namespace GameStates
{
    public class OnRetryEvent : Event
    {}
    public class EndGameState : StateMachineBehaviour
    {
        public SceneInspector endScene;
        public SceneInspector mainMenu;
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            GameManager.Instance.gameObject.ListenToEvent<OnStartGame>(OnRetryClick);
            AsyncOperation load = SceneManager.LoadSceneAsync(endScene.BuildIndex, LoadSceneMode.Single);
            load.completed += operation =>
            {
                ScreenEffect.FadeToColor(Color.clear, 0.3f);
            };
        }

        private void OnRetryClick(IEvent obj)
        {
            ScreenEffect.FadeToColor(Color.black, 0.3f, () =>
            {
                Destroy(GameManager.Instance.gameObject);
                SceneManager.LoadScene(mainMenu.BuildIndex, LoadSceneMode.Single);
            });
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            GameManager.Instance.gameObject.UnregisterToEvent<OnStartGame>(OnRetryClick);
        }
    }
}