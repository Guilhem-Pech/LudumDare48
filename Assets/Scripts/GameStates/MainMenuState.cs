﻿using System.Linq;
using GameEvent;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Event = GameEvent.Event;

namespace GameStates
{
    public class MainMenuState : StateMachineBehaviour
    {
        private Animator _animator;
        private static readonly int StartGame = Animator.StringToHash("StartGame");

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            ScreenEffect.FadeToColor(Color.clear, 0.3f);
            _animator = animator;
            GameManager.Instance.nextLevelIndex = 0;
            GameManager.Instance.gameObject.ListenToEvent<OnStartGame>(OnPlayerStart);
        }

        private void OnPlayerStart(IEvent obj)
        { 
            ScreenEffect.FadeToColor(Color.black, 0.3f, (() =>
            {
                SceneManager.LoadSceneAsync(GameManager.Instance.backgroundScene.BuildIndex, LoadSceneMode.Single)
                        .completed +=
                    operation =>
                    {
                        _animator.SetTrigger(StartGame);
                    };
            }));
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            GameManager.Instance.InitGame();
            GameManager.Instance.gameObject.UnregisterToEvent<OnStartGame>(OnPlayerStart);
            
        }
    }

    public class OnStartGame : Event
    {}
}