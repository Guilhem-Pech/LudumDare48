﻿using System.Linq;
using Entities.Cracks;
using Entities.Insects;
using GameEvent;
using Manager;
using Marvelous;
using Player;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using Utils;
using Event = GameEvent.Event;

namespace GameStates
{
    public class GameLoop : StateMachineBehaviour
    {
        private Animator _animator;
        private GameManager _manager;
        private static readonly int Win = Animator.StringToHash("Win");
        private static readonly int Lose = Animator.StringToHash("Lose");
        private static readonly int StartGame = Animator.StringToHash("StartGame");
        public PlayerController player;
        [ShowInInspector]
        private string curScene;
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            _animator = animator;
            _manager = GameManager.Instance;
            if (_manager.insectToCollect.Count <= 0)
            {
                _manager.InitGame(); //TODO Remove it, it's just for debug
            }
            _manager.gameObject.ListenToEvent<OnPlayerDie>(OnDie);
            _manager.gameObject.ListenToEvent<OnPlayerStompOnCrack>(OnStompOnCrack);
            _manager.gameObject.ListenToEvent<OnPlayerPickupInsect>(OnPlayerPickupInsect);
           if(_manager.localPlayer != null)
               Destroy(_manager.localPlayer.gameObject);

           if (_manager.nextLevelIndex < _manager.nbSceneInOrderBeforeRandom)
           {
               curScene = _manager.poolScene[0];
               _manager.poolScene.Remove(curScene);
           }
           else
           {
               curScene = _manager.poolScene[Random.Range(0, _manager.poolScene.Count)];
           }

           ++_manager.nextLevelIndex;
           var sceneLoad =SceneManager.LoadSceneAsync(curScene,LoadSceneMode.Additive);
           sceneLoad.completed += OnSceneLoadCompleted;
        }

        private void OnSceneLoadCompleted(AsyncOperation operation)
        {
            Assert.IsTrue(_manager.spawnpointList.Count > 0);
            // Player spawn
            int rSpawn = Random.Range(0, _manager.spawnpointList.Count);
            Instantiate(player.gameObject, _manager.spawnpointList[rSpawn].transform.position, Quaternion.identity);
            foreach (var sp in _manager.spawnpointList) Destroy(sp.gameObject);
            // Insect Spawn
            FindObjectsOfType<InsectRandom>().GetRandom().SpawnInsect();
            foreach (InsectRandom insectRandom in FindObjectsOfType<InsectRandom>()) Destroy(insectRandom.gameObject);
            // Exit Spawn
            CrackRandom exitPoint = FindObjectsOfType<CrackRandom>().GetRandom();
            exitPoint.SpawnExitPoint();
            foreach (CrackRandom exit in FindObjectsOfType<CrackRandom>())
            {
                if(exit.gameObject != exitPoint.gameObject)
                    Destroy(exit.gameObject);
            }
            GameManager.Instance.gameObject.SendEvent(new RefreshInsectHUD());
            ScreenEffect.FadeToColor(Color.clear, 0.3f);
        }

        private void OnPlayerPickupInsect(IEvent obj)
        {
            var pickupEvent = obj as OnPlayerPickupInsect;
            foreach (InsectSetting insectSetting in _manager.insectToCollect.Where(insectSetting => insectSetting.insectName == pickupEvent.insectName))
            {
                _manager.insectCollected.Add(insectSetting);
                break;
            }
            GameManager.Instance.gameObject.SendEvent(new RefreshInsectHUD());
            if (_manager.insectCollected.Count == _manager.insectToCollect.Count)
            {
                ScreenEffect.FadeToColor(Color.black, 0.4f, () =>
                {
                    _animator.SetTrigger(Win);
                });
            }
        }

        private void OnStompOnCrack(IEvent obj)
        {
            var stompEvent = obj as OnPlayerStompOnCrack;
            Debug.Log("ChangeMap");
            ScreenEffect.FadeToColor(Color.black, 0.2f, () =>
            {
                _animator.SetTrigger(StartGame);
            });
        }

        private void OnDie(IEvent obj)
        {
            var dieEvent = obj as OnPlayerDie;
            
            ScreenEffect.FadeToColor(Color.black, 0.4f, () =>
            {
                _animator.SetTrigger(Lose);
            });
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            SceneManager.UnloadSceneAsync(curScene);
            _manager.spawnpointList.Clear();
            _manager.gameObject.UnregisterToEvent<OnPlayerDie>(OnDie);
            _manager.gameObject.UnregisterToEvent<OnPlayerStompOnCrack>(OnStompOnCrack);
            _manager.gameObject.UnregisterToEvent<OnPlayerPickupInsect>(OnPlayerPickupInsect);
            
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
        }
    }

    public class OnPlayerStompOnCrack : Event
    {
        public PlayerController playerController;
    }
}