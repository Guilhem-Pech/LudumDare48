﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Utils
{
    public class AnimBehaviorUtil : StateMachineBehaviour
    {
        public bool needToFlipX = false;
        public bool needToFlipY = false;
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            if (animator.TryGetComponent(out SpriteRenderer renderer))
            {
                if(needToFlipX)
                    renderer.flipX = needToFlipX;
                if(needToFlipY && !renderer.flipY)
                {
                    renderer.GetComponent<Rigidbody2D>().position += Vector2.up;
                    renderer.flipY = needToFlipY;
                }
            }
            else
            {
                Debug.LogError($"COULD NOT FIND SPRITE RENDERER ON {animator}", this);
            }
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo,
            int layerIndex)
        {
            if (animator.TryGetComponent(out SpriteRenderer renderer))
            {
                if (needToFlipX)
                    renderer.flipX = false;
                if(needToFlipY && renderer.flipY)
                {
                    
                    renderer.flipY = false;
                }
            }
            else
            {
                Debug.LogError($"COULD NOT FIND SPRITE RENDERER ON {animator}", this);
            }
        }
    }
}