using System.Collections;
using System.Collections.Generic;
using GameEvent;
using GameStates;
using Manager;
using UnityEngine;
using UnityEngine.UI;

public class SimpleButtonScript : MonoBehaviour
{
    public Button button;

    private void Start()
    {
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        GameManager.Instance.gameObject.SendEvent(new OnStartGame());
    }
}
