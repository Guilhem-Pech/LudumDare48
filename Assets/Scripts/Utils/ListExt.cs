﻿using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine.Assertions;
using System.Security.Cryptography;
using Random = UnityEngine.Random;

namespace Utils
{
    public static class ListExt
    {
        public static T GetRandom<T>(this List<T> list)
        {
            Assert.IsTrue(list.Count > 0, "list.Count > 0");
            return list[Random.Range(0, list.Count)];
        }
        
        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}