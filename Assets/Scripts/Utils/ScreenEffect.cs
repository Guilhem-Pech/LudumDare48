﻿using System;
using System.Collections;
using Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    public class ScreenEffect
    {
        public static void FadeToColor(Color to, float time, Action callback = null)
        {
            GameManager.Instance.StartCoroutine(InternFade(to, time, GameManager.Instance.screenOver, callback));
        }

        private static IEnumerator InternFade(Color to, float time, Image image, Action callback)
        {
            Color initial = image.color;
            for(float i = 0; i < time; i+= Time.deltaTime)
            {
                image.color = Color.Lerp(initial, to, i / time);
                yield return null;
            }
            image.color = to;
            callback?.Invoke();
        }
    }
}