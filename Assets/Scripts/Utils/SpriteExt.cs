﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class SpriteExt
    {
        public static void FlickerSprite(this SpriteRenderer sp, Color flickerColor,float frequency, float howLong)
        {
            if(sp.TryGetComponent(out MonoBehaviour script))
            {
                script.StartCoroutine(FlickerCoroutine(sp, flickerColor, frequency, howLong));
            }
            else
            {
                Debug.LogError("Please attach a monobehavior to the object", sp.gameObject);
            }
        }

        private static IEnumerator FlickerCoroutine(SpriteRenderer sp, Color flickerColor, float frequency, float howLong)
        {
            bool showed = true;
            while (howLong >= 0)
            {
                if (showed)
                {
                    sp.color = flickerColor;
                    showed = false;
                }
                else
                {
                    sp.color = Color.white;
                    showed = true;
                }
                yield return new WaitForSeconds(frequency);
                howLong -= frequency;
            }
            sp.color = Color.white;
        }
    }
}