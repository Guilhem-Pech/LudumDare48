﻿using System;
using UnityEngine;

namespace Utils
{
    public static class VectorExt
    {
        
        public static bool MagnitudeLessThan(this Vector2  vec, float magnitude)
        {
            return vec.sqrMagnitude <= magnitude * magnitude;
        }
        public static bool IsZero(this Vector2  vec, float tolerance = 0.1f)
        {
            return MagnitudeLessThan(vec, tolerance);
        }
        
        public static bool IsEqual(this Vector2 a, Vector2 b, float tolerance = 0.1f)
        {
            return Math.Abs(a.x - b.x) < tolerance && Math.Abs(a.y - b.y) < tolerance;
        }
    }
}