using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    private static Sound instance = null;

    public static Sound Instance => instance;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    public AudioClip gameover ; //
    public AudioClip play ; //
    public AudioClip pause ; //
    public AudioClip wallHit1 ;
    public AudioClip wallHit2 ;
    public AudioClip unlockInsect ; //
    public AudioClip stomp ; //
    public AudioClip spike ; //
    public AudioClip rock ; //
    public AudioClip loseLife ; //
    public AudioClip getInsect ; //

    private void PlayAudio(AudioClip audioClip,float volume=1.0f)

    {
        float pitch = Random.Range(0.9f,1.1f) ;
        AudioSource.PlayClipAtPoint(audioClip,Camera.main.transform.position,volume);

    }

    public void PlayGameOver()
    {
        PlayAudio(gameover);
    }

    public void PlayPlay()
    {
        PlayAudio(play);
    }

    public void PlayPause()
    {
        PlayAudio(pause);
    }

    public void PlayWallHit()
    {
        if (Random.value>0.5f)
        {
            PlayAudio(wallHit1);
        }
        else
        {
            PlayAudio(wallHit2);
        }
    }

    public void PlayUnlockInsect()
    {
        PlayAudio(unlockInsect);
    }

    public void PlayStomp()
    {
        PlayAudio(stomp);
    }

    public void PlaySpike()
    {
        PlayAudio(spike);
    }

    public void PlayRock()
    {
        PlayAudio(rock);
    }

    public void PlayLoseLife()
    {
        PlayAudio(loseLife);
    }

    public void PlayGetInsect()
    {
        PlayAudio(getInsect,10.0f);

    }

}
