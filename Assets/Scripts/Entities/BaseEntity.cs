﻿using System;
using UnityEngine;

namespace Entities
{
    public class BaseEntity : MonoBehaviour
    {
        private void OnEnable()
        {
            var curpos = transform.position;
            curpos.z = 0;
            transform.position = curpos;
        }
    }
}