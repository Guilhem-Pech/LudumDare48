﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Player;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Assertions;

namespace Entities.Traps
{
    [RequireComponent(typeof(Collider2D))]
    public class FallingRocks : BaseEntity
    {
        public float vibrationSpeed = 40.0f;
        public float vibrationAmplitude = 0.03f;
        public float delayBeforeFall = 1f;
        public GameObject pillar;
        
        private SpriteRenderer _spriteRenderer;
        private Collider2D _collider2D;
        private static readonly int OnHit = Animator.StringToHash("OnHit");

        private void Start()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _collider2D = GetComponent<Collider2D>();
            _collider2D.isTrigger = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.TryGetComponent(out PlayerController ply))
            {
                Debug.Log("WATCH OUT!");
                Assert.IsNotNull(pillar);
                StartCoroutine(Vibrate(delayBeforeFall, vibrationAmplitude, vibrationSpeed, RockFall));
            }
        }

        private void RockFall()
        {
            ContactFilter2D noFilter = default;
            noFilter.NoFilter();
            List<Collider2D> res = new List<Collider2D>();
            if (_collider2D.OverlapCollider(noFilter, res) > 0)
            {
                foreach (Collider2D re in res)
                {
                    if (re.TryGetComponent(out PlayerController ply))
                    {
                        ply.RemoveLife();
                        ply.Rigidbody.velocity *= -1;
                    }
                }
            }
            
            Sound.Instance.PlayRock();

            Instantiate(pillar, transform.position, Quaternion.identity, transform.parent);
            ImpulseManager.instance.source.GenerateImpulseAt(transform.position, Vector3.down * 1f);
            Destroy(gameObject);
        }

        private IEnumerator Vibrate(float howLong, float amplipli , float frequency, Action callback)
        {
            Vector3 initPos = transform.position;
            float x = 0;
            while (howLong >= 0)
            {
                transform.position = initPos + new Vector3(math.sin(x * frequency), math.sin(x * frequency),0) * amplipli;
                howLong -= Time.deltaTime;
                x += Time.deltaTime;
                yield return null;
            }

            transform.position = initPos;
            callback.Invoke();
        }
        
    }
}