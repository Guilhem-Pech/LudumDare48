using System;
using Manager;
using Player;
using UnityEngine;

namespace Entities.Traps
{
    [RequireComponent(typeof(Collider2D))]
    public class Spikes : BaseEntity
    {
        public Animator animator;
        private Collider2D _collider2D;
        private static readonly int OnHit = Animator.StringToHash("OnHit");

        private void Start()
        {
            _collider2D = GetComponent<Collider2D>();
            animator = GetComponent<Animator>();
            _collider2D.isTrigger = true;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Debug.Log("Ouch");
            if (other.TryGetComponent(out PlayerController ply))
            {
                ply.RemoveLife();
                ImpulseManager.instance.source.GenerateImpulseAt(transform.position,Vector3.down * 0.8f);

                Sound.Instance.PlaySpike();
            }
        }
    }
}