using System;
using System.Security.Cryptography;
using Manager;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace Entities.Insects
{
    public class InsectRandom : MonoBehaviour
    {
        // Start is called before the first frame update
        private GameManager _manager;
        public void SpawnInsect()
        {
            _manager = GameManager.Instance;
            if (_manager == null || _manager.poolOfInsects == null || _manager.poolOfInsects.Count <= 0)
            {
                Debug.Log("No insects in pool !");
                return;
            }
            _manager.poolOfInsects.Shuffle();
            InsectSetting setting = _manager.poolOfInsects.GetRandom(); //TODO add probability
            GameObject insect = new GameObject(setting.name);
            Insect component = insect.AddComponent<Insect>();
            component.SetSetting(setting);
            insect.transform.parent = transform.parent;
            insect.transform.position = transform.position;
            Debug.Log($"SPAWNED {insect}", insect);
            Destroy(gameObject);
        }

        private void Start()
        {
            if (TryGetComponent(out SpriteRenderer renderer))
            {
                //Editor stuff don't need renderer
                Destroy(renderer);
            }
        }
    }
}
