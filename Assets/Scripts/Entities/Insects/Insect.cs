﻿using GameEvent;
using Manager;
using Player;
using Sirenix.OdinInspector;
using UnityEngine;
using Event = GameEvent.Event;

namespace Entities.Insects
{
    public class OnPlayerPickupInsect : Event
    {
        public string insectName;
        public PlayerController player;
        public Insect insect;
    }
    [RequireComponent(typeof(SpriteRenderer), typeof(Animator), typeof(BoxCollider2D))]
    public class Insect : BaseEntity
    {
        [ShowInInspector]
        private InsectSetting _setting;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.TryGetComponent(out PlayerController ply)) return;
            OnPlayerPickupInsect pickUpEvent = new OnPlayerPickupInsect {insectName = _setting.insectName, player = ply, insect = this};
            GameManager.Instance.gameObject.SendEvent<OnPlayerPickupInsect>(pickUpEvent);
        }

        public void SetSetting(InsectSetting setting)
        {
            _setting = setting;
            if (!TryGetComponent(out Animator animator))
            {
                animator = gameObject.AddComponent<Animator>();
            }
            animator.runtimeAnimatorController = setting.animator;
            if (!TryGetComponent(out BoxCollider2D collider))
            {
                collider = gameObject.AddComponent<BoxCollider2D>();
            }
            collider.size = new Vector2(1, 1);
            collider.isTrigger = true;
        }

        public InsectSetting GetSettings()
        {
            return _setting;
        }
    }
}