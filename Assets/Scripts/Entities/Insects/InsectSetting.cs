﻿using UnityEngine;

namespace Entities.Insects
{
    [CreateAssetMenu(fileName = "InsectName", menuName = "ScriptableObject/InsectSetting")]
    public class InsectSetting : ScriptableObject
    {
        public string insectName = "";
        public RuntimeAnimatorController animator;
        public Sprite initialSprite;
    }
}