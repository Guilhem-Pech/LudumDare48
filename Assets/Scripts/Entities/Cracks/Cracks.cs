﻿using System;
using UnityEngine;

namespace Entities.Cracks
{
    public class Cracks : BaseEntity
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
        
        private void OnTriggerExit2D(Collider2D other)
        {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
}