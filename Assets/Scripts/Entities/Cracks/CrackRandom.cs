using Entities.Insects;
using Manager;
using UnityEngine;

namespace Entities.Cracks
{
    public class CrackRandom : MonoBehaviour
    {
        private GameManager _manager;
        public void SpawnExitPoint()
        {
            gameObject.AddComponent<Cracks>();
            Destroy(this);
        }
    }
}
