﻿using System;
using Manager;
using UnityEngine;

namespace Entities.Spawnpoints
{
    public class Spawnpoint : MonoBehaviour
    {
        
        private void Awake()
        {
            if(GameManager.Instance != null)
                GameManager.Instance.spawnpointList.Add(this);
            GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}